
trash=main.bbl main.run.xml main.thm main.aux.bbl main.aux.blg main.synctex.gz

.DEFAULT_GOAL:= single

.PHONY: clean watch single

clean:
	latexmk -CA
	rm -rf $(trash)

single:
	latexmk -pdf -halt-on-error

watch:
	find . -name "*.tex" | entr make single


