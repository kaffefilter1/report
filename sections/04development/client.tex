\section{Parking Area Client}

\textit{This section will describe the implementation of PAC (Parking Area Client) introduced in \vref{sec:design:overview}.}

The PAC is where the computer vision methods described in \vref{sec:parkingdetection} will be deployed.
Because the methods mainly use python the PAC will too be written in python to ease development.

The PAC must have the following functionality.

\begin{enumerate}
    \item Capture images.
    \item Detect occupied spots using methods in \vref{sec:parkingdetection}.
    \item Format said images and send to the collector (see \vref{sec:collector}).
\end{enumerate}

Each of these functionalities will be implemented in a python module giving the following project structure.

\begin{verbatim}
pclient
    app.py           # Binds the 3 modules together
    client.py        # Implement collector client
    imgprovider.py   # Captures images
    __main__.py      # Program entry point
    predict.py       # Runs computer vision
\end{verbatim}

\subsection{Capturing images}

Capturing images may be implemented very differently depending on which image
source is used.
The image capture module offloads the task of capturing images to a extern python
module specified by the user.

This extern module can then load images from disk or camera as long as it implements
a standard interface.

The external image capture programs must implement a class named \texttt{ImgCapture} with the following methods.

\begin{itemize}
    \item \texttt{\_\_init\_\_(attr)} called when the program is run.
        Attr are options specified by the user.
    \item \texttt{Capture} called everytime a new image must be captured.
        Should return the image and captured timestamp.
\end{itemize}

During testing a simple module was created which loads images from a folder.

\begin{lstlisting}[language=python,caption={External python program capturing images}]
import cv2
import os
from datetime import datetime
import os.path as path

class ImgCapture:

    def __init__(self, attr):
        self.foldername = attr["folder"]

        # List imagefiles in folder
        self.files = os.listdir(self.foldername)
        self.files.sort()

        self.index = 0

    def capture(self):
        if self.index >= len(self.files):
            raise Exception("could not pull image: out of images")

        # Piece together path to imagefile
        fname = path.join(self.foldername, self.files[self.index])

        # Load image
        img = cv2.imread(fname)

        self.index += 1

        return img, datetime.now()
\end{lstlisting}

The client will load the user specified image module and run the capture function.

\begin{lstlisting}[language=python,caption={\texttt{imgprovider.py} calling the user specified module to capture images}]
import importlib.util as importutil

class Imgprov:
    def __init__(self, cfg):

        cfg = cfg["imgprov"]

        # Load image module
        spec = importutil.spec_from_file_location("module.name", cfg["modpath"])
        mod = importutil.module_from_spec(spec)
        spec.loader.exec_module(mod)

        # Create a instance of the class from the module
        self.img = mod.ImgCapture(cfg["modattr"])


    def getimage(self):
        # Run the capture function and return the result
        img, timestamp = self.img.capture()

        return img, timestamp
\end{lstlisting}

\subsection{Finding empty spots}

The predict module will run the collected images though a neural network trained
in \vref{lst:training}.
It also implements the image cropping described in \vref{subsec:cutout} for
cropping the cropped images.

% Cite https://OPENCV.org/
The cropping is done using OPENCV (OPEN source Computer Vision library) which is a common image manipulation library.
% Cite https://www.pyimagesearch.com/2014/08/25/4-point-OPENCV-getperspective-transform-example/
Because the marked spots can be a quadrilateral (polygon with four edges) and must be mapped to square images, 
a perspective transform must be performed.

This process requires two lists of image coordinates; \texttt{source}, \texttt{destination}.
\texttt{Source} specifies the 4 corners of the quadrilateral while \texttt{destination} is where the \texttt{source} coordinates
will be mapped in the resulting image.

While the \texttt{source} points will be loaded from the JSON spot definitions (see \vref{fig:datasetFormats}).
\texttt{Destination} is set as a rectangle with the dimensions of the \texttt{source} quadrilateral, but with a top-left corner in $(0,0)$.
The width/height of the \texttt{destination} rectangle is set as the higher of the two opposing sides of the \texttt{source} quadrilateral.

\begin{figure}[H]
    \centering

    \begin{tikzpicture}
        \begin{scope}
            % Rectangle
            \draw 
            (0, 2)
            -- node[below,sloped] {$2.83$} (2, 0)
            -- node[right] {$2$} (2, 2) 
            -- node[above,sloped] {$2.24$} (0, 3) node(O) {} node[above] {$(x, y)$}
            -- node[left] {$1$} cycle;

            % Point in origin
            \filldraw (O) circle (2pt);
        \end{scope}
        \begin{scope}[shift={(4,0)}]

            \draw
            (0, 0) 
            -- node[below] {$2.83$} (2.83, 0) 
            -- node[right] {$2$} (2.83, 2) 
            -- node[above] {$2.83$} (0, 2) node(O) {} node[above] {$(0, 0)$}
            -- node[left] {$2$} cycle;

            \filldraw (O) circle (2pt);
        \end{scope}
        % Labels
        \fill[black] (0, -0.7) node[right] {\texttt{Source}};
        \fill[black] (4, -0.7) node[right] {\texttt{Destination}};
    \end{tikzpicture}

    \caption{Warping polygon to rectangle, stretching the \texttt{source} quadrilateral to fit in a rectangle.}
\end{figure}

\texttt{Source} and \texttt{destination} is fed to the OPENCV functions \texttt{getPerspectiveTransform} and
\texttt{warpPerspective} which warps the image.

\begin{lstlisting}[language=python,caption={Warp using OPENCV.}]
# Create source coordinates from the point definition.
src = np.array([
    [tl["x"], tl["y"]],
    [bl["x"], bl["y"]],
    [br["x"], br["y"]],
    [tr["x"], tr["y"]]
], dtype="float32")

# Calculate destination coordinates.
dst = np.array([
    [0, 0],
    [0, maxHeight - 1],
    [maxWidth - 1, maxHeight - 1],
    [maxWidth - 1, 0]], dtype="float32")

# Create warp mapping.
M = cv2.getPerspectiveTransform(src, dst)

# Warp and scale the image.
warped = cv2.warpPerspective(img, M, (maxWidth, maxHeight))
\end{lstlisting}

The warped images can then be fed into the fastai library which will determine
whether it is occupied or not.

This is done repeatedly for every spot to generate a dictionary which maps spotid to a occupied boolean.

\begin{lstlisting}[language=python,caption={Running fastai library on all spots}]
def predict(self, img):
    # Create dictionary
    states = {}
    # For each point generate an output image
    for spot in self.pointdef:
        # Crop the spot
        warped = fourp_transform(img, spot["corners"])

        # Convert image to fastai image
        warped = cv2.cvtColor(warped, cv2.COLOR_BGR2RGB)
        warped = fv.Image(fv.pil2tensor(warped, dtype=np.float32).div_(255))

        # Predict
        cls, _, _ = self.learn.predict(warped)

        # Save whether the spot is taken in the dictionary
        states[point["id"]] = str(cls) == "occupied"

    # Return the created dictionary
    return states
\end{lstlisting}

\subsection{Reporting to collector}


\subsection{Connecting modules}

