\section{Parking spot detection}
\label{sec:parkingdetection}

{\itshape This section will cover different computer vision approaches used to detect parking lot occupancy.}

\subsection{Cutout method}
\label{subsec:cutout}

% Referer til inspiration https://towardsdatascience.com/find-where-to-park-in-real-time-using-opencv-and-tensorflow-4307a4c3da03

This method involves cutting the parking spots out of the captures image and then using computer pattern recognition on the cutout.

The program must therefore know the corners of each parking lot.
This can be done automatically, however in this project this will be done beforehand.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/b2.png}
	\caption{Still image of parking area at AAU}
	\label{fig:aaupark}
\end{figure}

\Vref{fig:aaupark} shows a picture taken at one of the parking areas at AAU.
Manually each spot can be marked out in a simple JSON (JavaScript Object Notation) as seen in \vref{fig:spacedef}.

\begin{figure}[H]
	\begin{verbatim}
{
    "id": 0,
    "corners": [
        { "x": 1429, "y": 2149 },
        { "x": 1040, "y": 2728 },
        { "x": 1623, "y": 2807 },
        { "x": 1914, "y": 2198 }
    ]
}
	\end{verbatim}
	\caption{Corner specification for at single parking slot.}
	\label{fig:spacedef}
\end{figure}

\Vref{fig:aaupark} also demonstrates one of the major challenges of this method, namely that the picture is captured from a perspective.
This means that cars can overlap other parking areas in the image (see second back row of cars), which complicates the pattern recognition.
Camera placement is therefore an important factor of the effectiveness of the detection.

Because of the perspective of the images the parking spots will no longer be rectangular nor will they be of equal size.
To make storage and pattern recognition simpler each spot image will be put through two preprocessors.

\begin{enumerate}
	\item The image will be perspective transformed to by pulling each corner.
	\item The image will be scaled to match the sizes of the other images.
		Because the images will have different aspect ratios, they can either be stretched or filled padded with black bars.
\end{enumerate}

This preprocessing is done using the opencv image processing library controlled from several python3 scripts.
A example of two cropped images can be seen in \vref{fig:carcut}.

\begin{figure}[H]
	\centering
	\begin{subfigure}[t]{0.3\textwidth}
		\includegraphics[width=\textwidth]{figures/carcut1.png}
		\caption{Car closer to camera thus less affected by other cars.}
		\label{fig:carcutnice}
	\end{subfigure}
	~
	\begin{subfigure}[t]{0.3\textwidth}
		\includegraphics[width=\textwidth]{figures/carcut2.png}
		\caption{Car further away from camera, increasing the effect over overlapping cars.}
		\label{fig:carcutbad}
	\end{subfigure}
	\caption{Two cropped images, with black bars added to give them the same size}
	\label{fig:carcut}
\end{figure}

% Find kilde
To detect whether these images contains cars or not a CNN (Convolutional Neural Network) was used.
These kind of neural networks are effective in video recognition and image classification.

Like a traditional neural network, CNN consist of input/output layers and a collection of hidden layers in between. 

% Cite http://yann.lecun.com/exdb/publis/pdf/lecun-89e.pdf
To detect features some of the hidden layers, called convoluted layers, consists of a collection of feature maps.
Each feature map can detect a single visual feature in the preceding layer.
Feature maps closer to the output detect more abstract features, while maps closer to the input detect simpler features.

For example: A feature map close to the input may detect corners or lines from pixels, while a more abstract feature map detects a composition of these simpler features.

% https://www.fast.ai/
To feasibility test the use of CNN networks the cutout images can be feeded into the library fastai.
This library automates and hides most of the CNN details, and generates competent neural networks.

Before the network can be used to detect occupancy it must be trained with a already categorised collection of images.
This collection will then be split up in training data and validation data, which must not overlap.

\begin{itemize}
	\item \textbf{Training} data is used to train the network by giving it input and correct output.
		The network parameters will then be adjusted to give it the correct output.
	\item \textbf{Validation} data is used to measure the accuracy of a trained network, by comparing the output with the correct output.
		It is important that these images are different than the ones used when training.
\end{itemize}

\subsubsection{Testing}

% Source cnrpark
To test this method the cnrpark dataset will be used. 
More specifically the pictures from camera 8.

This dataset includes a large collection of images captured from different angles and locations of the parking lots in the images.
It also includes the occupancy state for each parking spot in all the images.

Each spot in the dataset is defined as a rectangle roughly covering each parking spot.
This is different from the approach above where each corner is marked, and mapped to a rectangular image.
This difference can be seen in \vref{fig:cnrparkdiff}.

\begin{figure}
	\centering
	\begin{subfigure}[t]{0.3\textwidth}
		\includegraphics[width=\textwidth]{figures/ownregionmethod.png}
		\caption{Spot corners are strected to a rectangle}
	\end{subfigure}
	~
	\begin{subfigure}[t]{0.3\textwidth}
		\includegraphics[width=\textwidth]{figures/cnrreqgionmethod.png}
		\caption{Rectangle used by cnrpark which does not cover whole spot}
	\end{subfigure}
	\caption{Different ways to cut parking lots}
	\label{fig:cnrparkdiff}
\end{figure}

This test will measure the significance of the different spot placements and the best way to scale the images.
The following four test will therefore be done:

\begin{center}
\begin{tabular}{r | c c c c}
	                & Test 1 & Test 2 & Test 3  & Test 4 \\ \hline
	Preserve aspect & Yes    & No     & Yes     & No \\
	Spot definition & CNR    & CNR    & Corners & Corners
\end{tabular}
\end{center}

For each test a neural network will be trained the first half of the dataset (images from 2015).
Then the accuracy will be measured using the second half (images from 2016).

The accuracy will be measured as the percentage of images the neural network predicts correctly.

\Vref{fig:visiontestres} shows the results from the four tests.
Here it can be seen that almost all the networks have a tendency to mark empty spots as occupied.
This seems to have been fixed in the network generated in \textit{Test 2} which gives the overall best results.

The rawdata can be found in \vref{app:visioncutouttest}

\begin{figure}[H]
	\centering
	\begin{tikzpicture}
		\begin{axis}[
			ybar,
			enlargelimits=0.15,
			legend style={at={(0.5, -0.15)},
			anchor=north, legend columns=-1},
			ylabel={\%Accuracy},
			symbolic x coords={Test 1, Test 2, Test 3, Test 4},
			xtick=data,
			nodes near coords,
			nodes near coords align={vertical},
			]	
			\addplot coordinates {(Test 1, 68) (Test 2, 86) (Test 3, 69) (Test 4, 70)};
			\addplot coordinates {(Test 1, 99) (Test 2, 98) (Test 3, 99) (Test 4, 98)};
			\addplot coordinates {(Test 1, 86) (Test 2, 93) (Test 3, 87) (Test 4, 87)};
			\legend{Empty, Occupied, Total}
		\end{axis}
	\end{tikzpicture}
	\caption{Results from the four tests}
	\label{fig:visiontestres}
\end{figure}

