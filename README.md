# Report

## Writing Guidelines

-   Til at beskrive parkeringsting bruges parking area (flere parkeringspladser) & parking spot (én parkingsplads)

-   Acronyms are DLT (Done Like This)

-   All file names and labels should be camelCase

-   Language is en-US and en-GB

-   Use space as a thousand separator and dot as decimal separator, fx 1
    000.23

-   Dates follow [ISO8601](https://en.wikipedia.org/wiki/ISO_8601), fx yyyy-MM-dd and HH:mm

-   Citations should have quotation marks and be cursive, fx *"To be,
    or not to be"*

-   Special things should be written in cursive, fx *systemd*

-   Figure formats should be prioritized like this: Vector graphics
    (PDF, EPS) \> Lossless(PNG) \> Lossy(JPG)

-   Numbers are written with letter until 10, fx One, two, three\...,
    nine, 10, 11, 12, 13

-   Cybersecurity is written in two words, cyber security

-   Only write one sentence per line, this makes it easier to debug and
    comment

-   Citations are done using the \cite command

-   Cross-references are done using the varioref package. E.g. *"as seen in* \vref{fig:figur}*"*. To capitalize the first letter, use \Vref{} (only if the reference starts a sentence)

-   Labels must be prefixed with fig, eq, tab, lst, ch, or sec to define label type ([Source](https://en.wikibooks.org/wiki/LaTeX/Labels_and_Cross-referencing)). E.g. \label{tab:tabel2}

-   Pincode is written PIN code.

## Compile

Requires [latexmk](https://mg.readthedocs.io/latexmk.html).

`$ make single`

## Compile everytime you save

`$ make watch`

Requires [entr](https://github.com/clibs/entr), packaged in most distros.
